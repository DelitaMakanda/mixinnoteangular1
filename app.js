var app = require('./server/routes.js');

var server = app.listen(2300, function() {
    console.log('Listen to port %d', server.address().port);
});