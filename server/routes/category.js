var category = require('../models/category.js');

module.exports = function(app){
    app.get('/categories', function(req, res){
        res.json(category.all());
    });

    app.get('/categories/:id', function(req, res){
        var categoryId = parseInt(req.param('id'), 10);
        res.json(category.get(categoryId) || {});
    });
};