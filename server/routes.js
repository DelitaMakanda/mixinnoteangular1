var express = require('express');
var app = express();

require('./server.js')(app, express);

app.get('/', function(req, res){
    res.sendFile('index.html', {root: app.settings.views});
});

require('./routes/user.js')(app);
require('./routes/note.js')(app);
require('./routes/category.js')(app);


module.exports = app;
