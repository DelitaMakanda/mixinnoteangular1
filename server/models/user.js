var _ = require('lodash');

var users = [
    {
        "id": 1,
        "username": "Délita",
        "bio": "Une fille un peu maboule.",
        "site": "http://www.google.fr/",
        "location": "Paris"
    },
    {
        "id": 2,
        "username": "Jérome",
        "bio": "Un gars chelou avec un bonnet, des cheveux bouclés, un manteau gris, des baskets adidas qui aimait nager et qui n'aimait pas les sandwich à la dinde.",
        "site": "http://www.google.fr/",
        "location": "Paris"
    },
    {
        "id": 3,
        "username": "Malika",
        "bio": "Un collège de la Société Générale",
        "site": "http://www.google.fr/",
        "location": "Nanterre"
    },
    {
        "id": 4,
        "username": "Thomas",
        "bio": "Chibre et bukkake",
        "site": "http://www.google.fr/",
        "location": "Asnières"
    },
    {
        "id": 5,
        "username": "Maximillien",
        "bio": "Quelqu'un qui bavarde énormément",
        "site": "http://www.google.fr/",
        "location": "Versailles"
    },
    {
        "id": 6,
        "username": "Evelyne",
        "bio": "Une fille courageuse",
        "site": "http://www.google.fr/",
        "location": "Le Plessis-trevisse"
    }
];


module.exports = {
    get: function(id){
        return _.find(users, function(user){
            return user.id === id;
        });
    },
    all:function(){
        return users;
    }
}