angular.module('MixinNote')
.factory('User', function ($resource){
    return $resource('/users/:id');
});