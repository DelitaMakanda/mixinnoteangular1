angular.module('MixinNote')
.factory('Category', function ($resource){
    return $resource('/categories/:id');
});