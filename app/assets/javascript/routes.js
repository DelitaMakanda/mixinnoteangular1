angular.module('MixinNote').config(function($routeProvider){
    $routeProvider

    .when('/', {
        redirectTo: '/notes'
    })

    .when('/notes', {
        templateUrl: 'assets/templates/notes/index.html',
        controller: "NotesIndexController"
    })

    .when('/notes/:id', {
        templateUrl: 'assets/templates/notes/show.html',
        controller: "NotesShowController"
    })

    .when('/notes/:id/edit', {
        templateUrl: 'assets/templates/notes/edit.html',
        controller: "NotesEditController"
    })

    .when('/new', {
        templateUrl: 'assets/templates/notes/create.html',
        controller: "NotesCreateController"
    })

    .when('/users', {
        templateUrl: 'assets/templates/users/index.html',
        controller: "UsersIndexController"
    })

    .when('/users/:id', {
        templateUrl: 'assets/templates/users/show.html',
        controller: "UsersShowController"
    });
    
});