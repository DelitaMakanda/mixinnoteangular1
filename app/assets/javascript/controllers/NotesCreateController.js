angular.module('MixinNote')
.controller('NotesCreateController', function($scope, Note, $location){
    $scope.note = new Note();
    $scope.isSubmitted = false;
    
    $scope.saveNote = function(note) {
        $scope.isSubmitted = true;
        note.$save().then(function(){
            $location.path('/notes');
        }).catch(function(errors){
            //valid
        }).finally(function(){
            $scope.isSubmitted = false;
        });
    }
});
