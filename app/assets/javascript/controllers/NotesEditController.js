angular.module('MixinNote')
.controller('NotesEditController', function($scope, Note, Category, $routeParams, $location){
    $scope.note = Note.get({id: $routeParams.id});
    $scope.isSubmitted = false;
    $scope.categories = Category.query();

    console.log($scope.categories);
    
    $scope.saveNote = function(note) {
        $scope.isSubmitted = true;

        note.$update().finally(function(){
            $scope.isSubmitted = false;
            $location.path('/notes/' + note.id);
        });
    }

});
